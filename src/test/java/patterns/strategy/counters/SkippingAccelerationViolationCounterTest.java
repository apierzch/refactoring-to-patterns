package patterns.strategy.counters;

import org.junit.Test;

import static org.fest.assertions.Assertions.assertThat;


/**
 * Adam Pierzchała
 * 25/04/13
 */
public class SkippingAccelerationViolationCounterTest {


    private final IndexUpdateStrategy strategy = new SkippingIndexUpdateStrategy();

    @Test
    public void shouldIncrementOneAccelerationCounter() {
        // givern
        AccelerationViolationCounter counter = new AccelerationViolationCounter(strategy);

        // when
        counter.updateIndexes(-0.4d);
        counter.updateIndexes(-0.4d);

        // then
        assertThat(counter.getHardAccCount1()).isEqualTo(1);
        assertThat(counter.getHardAccCount2()).isEqualTo(0);
        assertThat(counter.getHardBreakingCount1()).isEqualTo(0);
        assertThat(counter.getHardBreakingCount2()).isEqualTo(0);
    }

    @Test
    public void shouldIncrementTwoAccelerationCounters() {
        // givern
        AccelerationViolationCounter counter = new AccelerationViolationCounter(strategy);

        // when
        counter.updateIndexes(-0.6d);
        counter.updateIndexes(-0.6d);

        // then
        assertThat(counter.getHardAccCount1()).isEqualTo(1);
        assertThat(counter.getHardAccCount2()).isEqualTo(1);
        assertThat(counter.getHardBreakingCount1()).isEqualTo(0);
        assertThat(counter.getHardBreakingCount2()).isEqualTo(0);
    }

    @Test
    public void shouldIncrementOneBreakingCounter() {
        // givern
        AccelerationViolationCounter counter = new AccelerationViolationCounter(strategy);

        // when
        counter.updateIndexes(0.5d);
        counter.updateIndexes(0.5d);

        // then
        assertThat(counter.getHardAccCount1()).isEqualTo(0);
        assertThat(counter.getHardAccCount2()).isEqualTo(0);
        assertThat(counter.getHardBreakingCount1()).isEqualTo(1);
        assertThat(counter.getHardBreakingCount2()).isEqualTo(0);
    }

    @Test
    public void shouldIncrementTwoBreakingCounters() {
        // givern
        AccelerationViolationCounter counter = new AccelerationViolationCounter(strategy);

        // when
        counter.updateIndexes(0.7d);
        counter.updateIndexes(0.7d);

        // then
        assertThat(counter.getHardAccCount1()).isEqualTo(0);
        assertThat(counter.getHardAccCount2()).isEqualTo(0);
        assertThat(counter.getHardBreakingCount1()).isEqualTo(1);
        assertThat(counter.getHardBreakingCount2()).isEqualTo(1);
    }
}
