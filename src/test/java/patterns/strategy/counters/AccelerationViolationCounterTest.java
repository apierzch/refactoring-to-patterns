package patterns.strategy.counters;

import org.junit.Test;

import static org.fest.assertions.Assertions.assertThat;


/**
 * Adam Pierzchała
 * 25/04/13
 */
public class AccelerationViolationCounterTest {

    private IndexUpdateStrategy strategy = new SimpleIndexUpdateStrategy();

    @Test
    public void shouldIncrementOneAccelerationCounterTwice() {
        // givern
        AccelerationViolationCounter counter = new AccelerationViolationCounter(strategy);

        // when
        counter.updateIndexes(-0.4d);
        counter.updateIndexes(-0.4d);

        // then
        assertThat(counter.getHardAccCount1()).isEqualTo(2);
        assertThat(counter.getHardAccCount2()).isEqualTo(0);
        assertThat(counter.getHardBreakingCount1()).isEqualTo(0);
        assertThat(counter.getHardBreakingCount2()).isEqualTo(0);
    }

    @Test
    public void shouldIncrementTwoAccelerationCountersTwice() {
        // givern
        AccelerationViolationCounter counter = new AccelerationViolationCounter(strategy);

        // when
        counter.updateIndexes(-0.6d);
        counter.updateIndexes(-0.6d);

        // then
        assertThat(counter.getHardAccCount1()).isEqualTo(2);
        assertThat(counter.getHardAccCount2()).isEqualTo(2);
        assertThat(counter.getHardBreakingCount1()).isEqualTo(0);
        assertThat(counter.getHardBreakingCount2()).isEqualTo(0);
    }

    @Test
    public void shouldIncrementOneBreakingCounterTwice() {
        // givern
        AccelerationViolationCounter counter = new AccelerationViolationCounter(strategy);

        // when
        counter.updateIndexes(0.5d);
        counter.updateIndexes(0.5d);

        // then
        assertThat(counter.getHardAccCount1()).isEqualTo(0);
        assertThat(counter.getHardAccCount2()).isEqualTo(0);
        assertThat(counter.getHardBreakingCount1()).isEqualTo(2);
        assertThat(counter.getHardBreakingCount2()).isEqualTo(0);
    }

    @Test
    public void shouldIncrementTwoBreakingCountersTwice() {
        // givern
        AccelerationViolationCounter counter = new AccelerationViolationCounter(strategy);

        // when
        counter.updateIndexes(0.7d);
        counter.updateIndexes(0.7d);

        // then
        assertThat(counter.getHardAccCount1()).isEqualTo(0);
        assertThat(counter.getHardAccCount2()).isEqualTo(0);
        assertThat(counter.getHardBreakingCount1()).isEqualTo(2);
        assertThat(counter.getHardBreakingCount2()).isEqualTo(2);
    }
}
