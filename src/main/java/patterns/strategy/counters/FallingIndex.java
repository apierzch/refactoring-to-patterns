package patterns.strategy.counters;

/**
 * Adam Pierzchała
 * 25/04/13
 */
public class FallingIndex extends Index {

    public FallingIndex(double threshold) {
        super(threshold);
    }

    public boolean violatesThreshold(double acceleration) {
        return acceleration < threshold;
    }
}
