package patterns.strategy.counters;

/**
 * Adam Pierzchała
 * 25/04/13
 */
public abstract class Index {
    private int value = 0;
    protected double threshold;

    public Index(double threshold) {
        this.threshold = threshold;
    }

    public void update() {
        value++;
    }

    public abstract boolean violatesThreshold(double acceleration);

    public int getValue() {
        return value;
    }
}
