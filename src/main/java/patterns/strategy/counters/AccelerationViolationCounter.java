package patterns.strategy.counters;

@SuppressWarnings("UnusedDeclaration")
public class AccelerationViolationCounter {

    private static final double HARD_ACC_THRESHOLD_1 = -0.3d;
    private static final double HARD_ACC_THRESHOLD_2 = -0.5d;
    private static final double HARD_BREAKING_THRESHOLD_1 = 0.4d;
    private static final double HARD_BREAKING_THRESHOLD_2 = 0.6d;

    private Index hardAccCount1 = new FallingIndex(HARD_ACC_THRESHOLD_1);
    private Index hardAccCount2 = new FallingIndex(HARD_ACC_THRESHOLD_2);
    private Index hardBreakingCount1 = new RisingIndex(HARD_BREAKING_THRESHOLD_1);
    private Index hardBreakingCount2 = new RisingIndex(HARD_BREAKING_THRESHOLD_2);

    private Index[] indexes = new Index[]{
            hardAccCount1,
            hardAccCount2,
            hardBreakingCount1,
            hardBreakingCount2
    };

    private IndexUpdateStrategy strategy;

    protected boolean isSkippingEnabled;

    public AccelerationViolationCounter(IndexUpdateStrategy strategy) {
        this.strategy = strategy;
    }

    public void updateIndexes(double acceleration) {
        for (Index index : indexes) {
            strategy.updateIndex(index, acceleration);
        }
    }

    public int getHardAccCount1() {
        return hardAccCount1.getValue();
    }

    public int getHardAccCount2() {
        return hardAccCount2.getValue();
    }

    public int getHardBreakingCount1() {
        return hardBreakingCount1.getValue();
    }

    public int getHardBreakingCount2() {
        return hardBreakingCount2.getValue();
    }

}
