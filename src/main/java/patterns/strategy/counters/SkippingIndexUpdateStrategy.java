package patterns.strategy.counters;

import java.util.HashMap;
import java.util.Map;

/**
 * Adam Pierzchała
 * 25/04/13
 */
public class SkippingIndexUpdateStrategy implements IndexUpdateStrategy {

    private Map<Index, Boolean> skipIndex = new HashMap<Index, Boolean>();

    @Override
    public void updateIndex(Index index, double acceleration) {
        if (index.violatesThreshold(acceleration)) {
            if (!skipIndex.containsKey(index) || !skipIndex.get(index)) {
                index.update();
                skipIndex.put(index, true);
            }
        } else {
            skipIndex.put(index, false);
        }
    }
}
