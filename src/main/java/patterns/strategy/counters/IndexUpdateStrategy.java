package patterns.strategy.counters;

/**
 * Adam Pierzchała
 * 25/04/13
 */
public interface IndexUpdateStrategy {

    void updateIndex(Index index, double acceleration);
}
