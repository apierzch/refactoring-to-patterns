package patterns.strategy.counters;

/**
 * Adam Pierzchała
 * 25/04/13
 */
public class SimpleIndexUpdateStrategy implements IndexUpdateStrategy {
    @Override
    public void updateIndex(Index index, double acceleration) {
        if (index.violatesThreshold(acceleration)) {
            index.update();
        }
    }
}
